# Using the official NVIDIA cuda image as starting point
FROM nvidia/cuda:10.0-devel

# Create user for login on ctl
RUN adduser --disabled-login --gecos '' slurm_user
RUN echo "slurm_user:sdu112" | chpasswd

# Set the frontend as 'noninteractive', telling the shell that there is no user to interact with
ENV DEBIAN_FRONTEND=noninteractive

#Copy the NVIDIA cudnn library to the /home folder
COPY libcudnn7_7.6.4.38-1+cuda10.0_amd64.deb /home/ 

RUN apt-get update

# Installs utils
RUN apt-get install -y git vim less net-tools gcc make perl wget iputils-ping
#dnsutils

# Install SLURM
RUN apt-get install -y munge slurm-wlm 

# Install lmod
RUN apt-get install -y gettext libz-dev zlib1g-dev liblua5.2-dev liblua5.1-0-dev luarocks libglfw3-dev

# Install module utils
RUN apt-get install -y python python-dev python-pip

# ?
RUN export PATH=/usr/local/cuda/bin:$PATH && export LD_LIBRARY_PATH=/usr/local/cuda/lib64:$LD_LIBRARY_PATH

# Installs luaposix and luafilesystem for lmod
RUN luarocks install luaposix; luarocks install luafilesystem

# Install lmod
RUN apt-get install -y lmod

# Copy the pre-created munge.key, chaning the ownership and the permissions
COPY /munge.key /etc/munge
RUN chown munge:munge /etc/munge/munge.key
RUN chmod 400 /etc/munge/munge.key

# Copy the slurm configs, changing the ownership and the permissions
RUN ln -s /mnt/ceph/slurmSettings/slurm_conf/slurm.conf /etc/slurm-llnl/slurm.conf
RUN ln -s /mnt/ceph/slurmSettings/slurm_conf/gres.conf /etc/slurm-llnl/gres.conf

# Clone the Cuda samples for cuda version 10.0 and making the files
RUN git clone --branch v10.0 https://github.com/NVIDIA/cuda-samples.git; cd cuda-samples; make; cd ..

#Nedenstående virker ikke ordentligt. Nu virker det fra den helt rigtige mappe.
# Make symbolic links, to make lmod work
RUN ln -s /usr/include/lua5.3/ /usr/include/lua5.1; ln -s /usr/lib/x86_64-linux-gnu/liblua5.2-posix.so.1.0.0 /usr/share/lua/5.2/posix/posix.so;ln -s /usr/lib/x86_64-linux-gnu/lua/5.2/posix_c.so /usr/lib/x86_64-linux-gnu/lua/5.2/posix.so
# Uncomment the following line, when lmod works.
#RUN source /etc/profile.d/lmod.sh

# Make the NVIDIA cudnn package and symbolic links
RUN cd /home; dpkg -i libcudnn7_7.6.4.38-1+cuda10.0_amd64.deb && rm libcudnn7_7.6.4.38-1+cuda10.0_amd64.deb
RUN cd /usr/local; ln -s cuda nvidia

# Upgrade pip and installing tensorflow-gpu
# RUN python3 -m pip install setuptools==41.0.0
RUN python -m pip install --upgrade pip
RUN python -m pip install tensorflow-gpu

# Installing pytorch
RUN python -m pip install numpy torch torchvision

# Installing plugins
RUN apt-get install -y python-tk
RUN python -m pip install audioread hmmlearn kafka-python matplotlib numpy opencv-contrib-python pandas panda prettytable PyWavelets scikit-learn scikit-image scipy seaborn simplejson sklearn

# Install supervisord and start the container
RUN apt-get install -y supervisor
COPY supervisord/compute.conf /etc/supervisord.conf
RUN mkdir -p /var/run/sshd /var/log/supervisor

#CMD /etc/init.d/munge restart && supervisord -c /etc/supervisord.conf -n
CMD /etc/init.d/munge restart && sleep 604800
#CMD /etc/init.d/munge restart; /etc/init.d/slurmd restart; sleep 604800
#CMD /etc/init.d/munge restart; slurmd -D
