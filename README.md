# Intro

# lmod

## Commands

**LIST MODULES**
`$ module avail`

**LOAD MODULES**
`$ module load example-command/example-version`
OR
`$ module load example-command`

**SEE LOADED MODULES**
`$ module list`

**UNLOAD MODULES**
`$ module unload example-command`
